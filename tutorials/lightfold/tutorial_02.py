import sys
import numpy as np
sys.path.append('../../')

from mindspore import context, nn

from sponge import Sponge, WithEnergyCell
from sponge.system.toys import BenzRigidBody
from sponge.potential import ForceField
from sponge.callback import RunInfo, WriteH5MD

PI = 3.1415927
ATOM_MASS = {'C': 12.01, 'H': 1.008}
context.set_context(mode=context.GRAPH_MODE, device_target="GPU")

mol2_file = '../mol2/benzene_xrd.mol2'
# (R, 3)
rigid_index = np.array([[0, 2, 4]]*32) + (np.arange(32)*12)[:,None]
rigid = BenzRigidBody(mol2_file, index=rigid_index, length_unit='A')
potential = ForceField(rigid, parameters=['amber.gaff'], rebuild_system=False)
opt = nn.Adam((rigid.quat, ), 1e-2)
sim = WithEnergyCell(rigid, potential)
minimization = Sponge(sim, optimizer=opt)

run_info = RunInfo(10)
rigid_h5md = WriteH5MD(rigid, 'benz_rigid.h5md', save_freq=10)
minimization.run(50, callbacks=[run_info, rigid_h5md])
# Copyright 2021-2023 @ Shenzhen Bay Laboratory &
#                       Peking University &
#                       Huawei Technologies Co., Ltd
#
# This code is a part of MindSPONGE:
# MindSpore Simulation Package tOwards Next Generation molecular modelling.
#
# MindSPONGE is open-source software based on the AI-framework:
# MindSpore (https://www.mindspore.cn/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import os
os.environ['GLOG_v'] = '4'
import numpy as np
from mindspore import context

if __name__ == "__main__":

    import sys
    sys.path.append('../../')
    from sponge import RigidBody
    from sponge.core import Rigid
    from sponge.optimizer import SteepestDescent
    from sponge.potential.energy import RigidEnergy
    from sponge.callback import RunInfo, RigidH5MD


    context.set_context(mode=context.GRAPH_MODE, device_target="GPU")

    # A very simple 4-rigid-atom case
    # fix_mask = np.array([1, 0, 0, 1], np.int32)
    # system = RigidBody(pdb='../pdb/case1.pdb', fix_mask=fix_mask, pdb_name='./rigid.pdb', length_unit='A')

    # 157-rigid-atom case
    fix_mask = np.zeros((157, ))
    fix_mask[:10] = 1
    fix_mask[-10:] = 1
    system = RigidBody(pdb='../pdb/case2.pdb', fix_mask=fix_mask, pdb_name='./rigid.pdb', length_unit='A')

    # 648-rigid-atom case
    # fix_mask = np.zeros((648, ))
    # fix_mask[:10] = 1
    # fix_mask[-10:] = 1
    # system = RigidBody(pdb='../pdb/T1095-D1.pdb', fix_mask=fix_mask, pdb_name='./rigid.pdb', length_unit='A')

    energy = RigidEnergy(fix_mask=fix_mask)
    opt = SteepestDescent(system.trainable_params(), 1e-02)
    
    md = Rigid(system, energy, opt)
    run_info = RunInfo(10)
    cb_h5md = RigidH5MD(system, 'tutorial_01.h5md', save_freq=1, length_unit='A')

    md.run(200, callbacks=[run_info, cb_h5md])


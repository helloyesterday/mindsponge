import os
os.environ['GLOG_v']='4'

import numpy as np
from mindspore import context, ops

import sys
sys.path.insert(0, '../..')

from sponge import Sponge
from sponge import Molecule
from sponge import ForceFieldBase
from sponge import UpdaterMD

from sponge.potential import BondEnergy, AngleEnergy
from sponge.function import VelocityGenerator

context.set_context(mode=context.PYNATIVE_MODE, device_target="GPU")
##################################### MD Calculation ################################
system = Molecule(
    atoms=['O', 'H', 'H'],
    coordinate=[[0, 0, 0], [0.1, 0, 0], [-0.0333, 0.0943, 0]],
    bonds=[[[0, 1], [0, 2]]],
)

bond_energy = BondEnergy(
    index=system.bonds,
    force_constant=[[345000, 345000]],
    bond_length=[[0.1, 0.1]],
)

angle_energy = AngleEnergy(
    index=[[1, 0, 2]],
    force_constant=[[383]],
    bond_angle=[[109.47 / 180 * np.pi]],
)

potential = ForceFieldBase(energy=[bond_energy, angle_energy])

vgen = VelocityGenerator(300)
velocity = vgen(system.shape, system.atom_mass)

updater = UpdaterMD(
    system=system,
    time_step=1e-3,
    velocity=velocity,
    integrator='leap_frog',
    temperature=300,
    thermostat='langevin',
)

md = Sponge(system, potential, updater)
bond_energy.force_constant.requires_grad = True

##################################### Meta Optimization ################################
def variable(value, steps=10):
    ops.assign(bond_energy.force_constant, value)
    bond_length = 0.
    for _ in range(steps):    
        md.run(1)
        crd = md._system.get_coordinate()
        bond_length += 0.05 * (crd[:, 1] - crd[:, 0]).norm() + 0.05 * (crd[:, 2] - crd[:, 0]).norm()
    return bond_length

def loss(bond_length):
    return (bond_length - 0.09) ** 2

def run(epoch=10, learning_rate=1000.):
    print ('Step 0: {}'.format(bond_energy.force_constant.sum()))
    for _ in range(epoch):
        var = variable(bond_energy.force_constant, steps=10)
        gLossLambda = ops.grad(loss)(var)
        value = bond_energy.force_constant.value() - gLossLambda * learning_rate
        ops.assign(bond_energy.force_constant, value)
        print ('Step {}: {}'.format(_+1, bond_energy.force_constant.sum()))

run(10)

# Step 0: 690000.0
# Step 1: 689959.3
# Step 2: 689917.56
# Step 3: 689876.9
# Step 4: 689835.3
# Step 5: 689794.3
# Step 6: 689752.75
# Step 7: 689711.6
# Step 8: 689670.1
# Step 9: 689628.3
# Step 10: 689586.75
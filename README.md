# MindSPONGE

## 介绍

MindSPONGE (MindSpore Simulation Package tOwards Next Generation molecular modelling)

一款基于MindSpore开发的模块化、高通量、端到端可微的下一代智能分子模拟程序库

本程序由深圳湾实验室、华为MindSpore开发团队、北京大学、昌平实验室共同开发

开发人员：杨奕，陈迪青，张骏，夏义杰

联系方式：yangyi@szbl.ac.cn

## 软件架构

<div align=left>
<img src="docs/mindsponge.png" alt="MindSPONGE Architecture" width="600"/>
</div>

## 安装教程

- MindSPONGE基于华为全场景人工智能框架MindSpore开发，使用前请先安装MindSpore：<https://mindspore.cn/>

- 安装MindSPONGE前需要先安装依赖环境：

```bash
pip install -r requirements.txt
```

- 编译程序

```bash
export CUDA_PATH={your_cuda_path}
bash build.sh -e gpu -j32
```

- 编译好的whl包在dist目录下，进入后可以使用pip进行安装：

```bash
pip install mindsponge-[gpu|ascend]
```

## 使用说明

使用教程在tutorials目录：

### 基础教程：

tutorial_b01.py: 手工创建一个模拟体系

tutorial_b02.py: 通过模板和力场参数创建一个模拟体系

tutorial_b03.py: 编辑体系与能量极小化

tutorial_b04.py: 带有偏向势的MD模拟

tutorial_b05.py: 周期性边界条件下的MD模拟

tutorial_b06.py: 蛋白质分子的能量极小化和MD模拟

tutorial_b07.py: LINCS约束算法

### 高级教程：

tutorial_a01.py: Metrics与集成变量（collective variables）

tutorial_a02.py: 偏向势（bias potential）与埋拓动力学（Metadynamics）

tutorial_a03.py: 能量包装器（Energy wrapper）与温度积分增强抽样（integrated tempering sampling）

tutorial_a04.py: 混合型增强抽样与MetaITS

### 博客教程：

[MindSponge博客教程合集 \[持续更新...\]](https://www.cnblogs.com/dechinphy/collections/5620)

## 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


# MindSPONGE

## Description

MindSPONGE (MindSpore Simulation Package tOwards Next Generation molecular modelling)

## Software Architecture

<div align=left>
<img src="docs/mindsponge_en.png" alt="MindSPONGE Architecture" width="600"/>
</div>

## Installation

This code was developed base on the AI framework MindSpore: <https://mindspore.cn/en>

- Install the dependency libraries:

```bash
pip install -r requirements.txt
```

- Compile the codes:

```bash
export CUDA_PATH={your_cuda_path}
bash build.sh -e gpu -j32
```

- The compiled whl package is in the dist directory. Use pip to install:

```bash
pip install mindsponge-[gpu|ascend]
```

## Instructions

The tutorials for user are in the "tutorials" directory：

Basic tutorials:

tutorial_b01.py: Create a simple simulation system manually.

tutorial_b02.py: Create a simple simulation system using template and parameters file.

tutorial_b03.py: Edit system and minimization.

tutorial_b04.py: MD simulation with bias potential.

tutorial_b05.py: MD simulation with periodic boundary condition.

tutorial_b06.py: Minimization and MD simulation of protein molecule.

tutorial_b07.py: Constraint and LINCS.

Advanced tutorials:

tutorial_a01.py: Metrics and collective variables (CVs).

tutorial_a02.py: Bias potential and metadynamics (MetaD)

tutorial_a03.py: Energy wrapper and integrated tempering sampling (ITS).

tutorial_a04.py: Hybrid enhanced sampling and MetaITS

## Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


## Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
